<!-- Exercice 1 Déclarer un tableau avec des nombres  random.
-Calculer la moyenne -Si le chiffre est supérieur à 10 noté un message à l’écran
-Si le chiffre est inférieur à 10 marqué un message en rouge -->

<?php

$notes = array();

for ($i=0; $i < 10; $i++) {
  $random = rand (0, 20);
  array_push($notes, $random);
  if ($random < 10) {
    echo $random . "il est inférieur à 10 </br>";
  }else{
    echo $random . "<p style="color:red;"> il est supérieur à 10 </p>";
  }


};
// Ex 1 ok //


// Déclarer un tableau avec des nombres  random. //
//  Calculer la moyenne -Afficher le résultat dans un h1 Html //

$random = array_sum($notes)/count($notes);
echo $random . '<h1 style="color:blue;"> La moyenne </h1>';

?>

<!-- Correction exo
php
$_nbValue = 10;
$_array = array();
for ($i = 0; $i<$_nbValue; $i++){
array_push($_array, rand(0, 100));
}

$_sumValue = 0;
for ($i = 0; $i < $_nbValue; $i++){
if($_array[$i] < 10){
echo $_array[$i] . ” est inférieur à 10 <br>“;
}else{
echo $_array[$i] . ” est supérieur à 10 <br>“;
}
$_sumValue = $_sumValue + $_array[$i];
}

if($_nbValue > 0){
echo “La moyenne est : ” . $_sumValue/$_nbValue . “<br>“;
}

echo array_sum($_array)/count($_array);

?>
-->


<!-- new exo héritages
Avec de l’héritage faite la description de plusieurs formes
(rond, triangle, carré, ellipse) exemple
$rond = new Rond(); $rond->print(); // Affiche : ceci est un rond -->

<?php

class Icons {
  protected $icons ='icons';
  public function printIcons() {
    echo "<h1> Ceci est un $this->icons </h1>";
  }
};

class Rond extends Icons {
  protected $icons = "rond";
};

class Triangle extends Icons {
  protected $icons = "triangle";
};

class Carre extends Icons {
  protected $icons = "carré";
};

class Ellipse extends Icons {
  protected $icons = "Ellipse";
};

$icons = new Icons();
$rond = new Rond();
$triangle = new Triangle();
$carré = new Carre();
$ellipse = new Ellipse();
$rond->printIcons();
$triangle->printIcons();
$carré->printIcons();
$ellipse->printIcons();
?>

<!-- Exo 3 Exercice 2 (plus compliqué) Un éleveur de volaille reçoit d’un fournisseur de jeunes canards et de jeunes poulets qu’il élève jusqu’à ce qu’ils aient la taille nécessaire à leur commercialisation.
Une volaille est caractérisée par son poids et un numéro d’identification reporté sur une bague qu’elle porte a sa petite patte. Les volailles arrivent à l’élevage à l’âge de trois semaines.
Elles sont baguées et enregistrées dans le système informatique.Il y a deux sortes de volailles : des canards et des poulets. Le prix du canard et celui du poulet sont deux prix différents, exprimés en euros par kilo.
En revanche, le prix est le même pour tous les individus de la même espèce. Ce prix varie chaque jour. Le poids auquel on abat les bêtes est différents pour les canards et les poulets, mais c’est le même pour tous les poulets (respectivement, tous les canards).
1.Écrivez une classe des volailles avec deux sous-classes des poulets et des canards. Il faut pouvoir enregistrer les prix du jours, les poids d’abbatage, le poids d’une volaille donnée.
2.Écrivez une classe permettant de représenter l’ensemble des animaux de l’élevage au moyen d’un tableau et de savoir le nombre de volailles reçues et le type de chaque volaille. On considérera que l'éleveur ne peut pas recevoir au maximum que 200 volailles à la fois.
.Écrire une méthode qui permet de calculer le prix total de toutes les volailles que l'éleveur possède. -->

<?php

class Poultry {
  protected $_dailyPrice = 50; // price of the per kg
  protected $_slaugtherWeight = 3; // weight of slaugther
  protected $_poultryWeight = 2; // weight of the Poultry
  protected $_type = 'Poultry'; // type of poultry
};

class Chicken extends Poultry {   // defines the values for chicken
  protected $_dailyPrice = 7;
  protected $_slaugtherWeight = 2;
  protected $_poultryWeight = 2;
  public function __construct($_dailyPrice, $_slaugtherWeight, $_poultryWeight){
    $this->_dailyPrice=$_dailyPrice;
    $this->_slaugtherWeight=$_slaugtherWeight;
    $this->_poultryWeight=$_poultryWeight; // la fonction constructeur permet d'hérité des valeurs
  }
  public function print() { // call result, la chaîne de caractére demander, $this renvoi aux déclaration de la function construct
    echo "<p> Prix du jour : $this->_dailyPrice, Poid d'abbatage : $this->_slaugtherWeight,
    Poid de l'animal : $this->_poultryWeight, type : Chicken </p>";
  }
};

class Duck extends Poultry {    // defines the values for duck
  protected $_dailyPrice = 6;
  protected $_slaugtherWeight = 2;
  protected $_poultryWeight = 2;
  public function __construct($_dailyPrice, $_slaugtherWeight, $_poultryWeight){
    $this->_dailyPrice=$_dailyPrice;
    $this->_slaugtherWeight=$_slaugtherWeight;
    $this->_poultryWeight=$_poultryWeight;
  }
  public function print() {
    echo "<p> Prix du jour : $this->_dailyPrice, Poid d'abbatage : $this->_slaugtherWeight,
    Poid de l'animal : $this->_poultryWeight, type : Duck </p>";
  }
};

class Farm {    // Déclaration des valeurs et calculs puis push array
  public function print() {
    $_nChick = rand(0, 200);
    $_dpChick = (rand(500,1000))/100;
    $_swChick = (rand(20,40))/10;
    $_array = [];
    for ($i=0; $i < $_nChick; $i++) {
      $_pwChick = (rand(10,30))/10;
      $_Chick = new Chicken($_dpChick, $_swChick, $_pwChick);
      $_Chick->print();
      array_push($_array, $_chick);
    }
    $_nDuck = rand(0,200-$_Nchick);
    $_dpDuck = (rand(500,1000))/100;
    $_swDuck = (rand(20,40))/10;
    for ($i=0; $i < $_nDuck; $i++) {
      $_pwChick = (rand(10,30))/10;
      $_Duck = new Duck($_dpDuck, $_swDuck, $_pwDuck);
      $_Duck ->print();
      array_push($_array, $_Chick);
    }
    echo "<br> <p> $_nChick Chickens </p> <p> $_nDuck Ducks </p>";
    $_totalPrice = ($_nChick*($_dpChick*$_swChick))+
    ($_nDuck*($_dpDuck*$_swDuck));
    echo "<p> Total value: $_totalPrice";
  }
};

$_Farm = new Farm();
$_Farm->print();

?>


// Fin de l’excercice sur les volailles:
// 1 Créer un fermier (qui va gérer le hangar), un comptable
// (qui veut savoir quel est le prix des poulets ou canard, toutes les données financières),
// charcutiers (peut récupérer les poulets à abattre).
// 2 Le personnel à une action :
// exemple :
// - Fermier : ajouter des poulets
// - comptable : déterminer le prix du jour
//  - charcutiers : vient acheter les poulets à abattre
// 3 Bonus : Faire une coopération de ferme

<?php

class Poultry {
  protected $_type;
  protected $_dailyPrice;
  protected $_slaugtherWeight;
  protected $_weight;
  public function __construct($_type, $_dailyPrice, $_slaugtherWeight, $_weight) {
    $this->_type=$_type;
    $this->_dailyPrice=$_dailyPrice;
    $this->_slaugtherWeight=$_slaugtherWeight;
    $this->_weight=$_weight;
  }

  // funcition get = rappel une class en objet

  public function getType($type) {
    $this->_type=$type;
  }

  public function getDailyPrice($dailyPrice) {
    $this->_dailyPrice=$dailyPrice;
  }

  public function getSlaugtherWeight($slaugtherWeight) {
    $this->_slaugtherWeight=$slaugtherWeight;
  }

  public function getWeight($weight) {
    $this->_weight=$weight;
  }
};

class Chicken extends Poultry {
  protected $_type= 'Poulets';
};

class Duck extends Poultry {
  protected $_type = 'Canards';
};

// abstract class permet de donner une abstract function afin de faire réaliser
// une action a des roles avec des conditions en l'indiquant dans un tableau

abstract class Staff {
  protected $_role;
  public abstract function action($getDailyPriceChicken, $getStorage, $getDailyPriceDuck)
};

class Farmer extends Staff {    // créer le fermier, son rôle et les var qu'il demandent
  protected $_role='Fermier';
  public function action($getDailyPriceChicken, $getStorage, $getDailyPriceDuck) {
    $_slaugtherWeightChicken = (rand(500,800))/100;
    $_slaugtherWeightDuck = (rand(500, 800))/100;
    $_numbersPoultry = rand(0, 200);
    for ($i=0; $i < $_numbersPoultry; $i++) {
      $_isChicken = rand(0, 1) ? true : false
      if ($_isChicken) {
        $_numbersChicken++
        $_chickenWeight = (rand(400, 800))/100;
        $_chicken = new Chicken ("chicken", $getDailyPriceChicken, $_slaugtherWeightChicken, $_chickenWeight);
        array_push($getStorage, $_chicken);

      } else {

        $_numbersDuck++
        $_duckWeight = (rand(400, 800))/100;
        $_duck = new Duck ("duck", $getDailyPriceDuck, $_slaugtherWeightDuck, $_duckWeight);
        array_push($getStorage, $_duck);
      }
    }
  }
  public function print() {
    echo "<p> Nombre de poulets : $this->_numbersChicken, Nombre de canards : $this->_numbersDuck </p>";
  }
};

class Accountant extends Staff {    // Créer le comptable, son rôle et les var qu'il demande
  protected $_role='Comptable';
  public function action($getDailyPriceChicken, $getStorage, $getDailyPriceDuck) {
    $_chickenPrice = rand(6, 15);
    $_duckPrice = rand(12, 22);
    $_numbersPoultry = rand(10, 200);
  }
  public function print() {
    echo "<p> Prix du jour pour les poulets : $this->_chickenPrice, Prix du jour pour kes canards : $this->_duckPrice </p>";
  }
};

//, le boucher, client : role -> acheter les poulets en stock}







$Farmer = new Farmer();
$Accountant = new Accountant();



?>


<!-- Exercice 1:
Dans une société, un employé est décrit par les membres suivants :
Attributs :
Nom ; Age ; salaire.
Constructeur :
avec trois paramètres
Méthodes :
Augmentation(…) ;
afficher() ;
calculeSalaire() ;
Un technicien est décrit en plus par l’attribut : grade et la méthode prime( ) et de la méthode calculeSalaire().
Si grade=1 alors Prime= 100
Si grade=2 alors Prime= 200
Si grade=3 alors Prime= 300
Travail à faire :
1-      Ecrire la classe Employé.
2-      Ecrire la classe Technicien.
3-      Ecrire un programme qui saisie un employé puis un technicien et affiche leurs informations avant et après augmentation de leurs salaires. -->

<?php

class Employees     // déclare les var des emplyées //
{
  protected $_type;
  protected $_firstName;
  protected $_lastName;
  protected $_age;
  protected $_salary;
  protected $_increase;
  protected $_salaryUp;

  public function __construct()   // donne un employé , déclare un nouveau type //
  {
    $this->randomFirstName();
    $this->randomLastName();
    $this->_type = "worker";
    $this->randomAge();
    $this->increase();

  }

  public function increase(){
    $this->_increase = intval(rand(50,200));
  }

  public function salaryUp(){                 // Déclare l'action d'up un salarié, avec le résultat //
    $_yes = (rand(0,1) == 0 ? true : false);
    if($_yes){
      $this->_salaryUp = $this->_salary + $this->_increase;
      echo "Augmentation de : ". $this->_increase;
      echo "<br>";
      echo "Salaire après augmentation : ". $this->_salaryUp;

    }else {
      echo "Augmentation : non";
    }
  }

  public function randomFirstName()     // déclare un résultat random dans un tableau //
  {
    $_arrayName = ["Anthony","Gauthier","Jayson","Chris","Julien"];
    $_rand = rand(0,(count($_arrayName)-1));
    $this->_firstName = $_arrayName[$_rand];
  }
  public function randomLastName()
  {
    $_arrayName = ["SARAPATA","STALEN","PREUVOT","LAMIAUT","MAILLE"];
    $_rand = rand(0,(count($_arrayName)-1));
    $this->_lastName = $_arrayName[$_rand];
  }

  public function randomAge() {
    $this->_age = rand(18,65);
  }


  public function randomSalary($_randomSalary) {      // les fonctions get et set; appelle et affiche les valeurs //
    $this->_salary = $_randomSalary;
  }


  public function getType() {
    return $this->_type;
  }

  public function setType($_type) {
    $this->_type = $_type;
  }

  public function getFirstName() {
    return $this->_firstName;
  }

  public function getLastName() {
    return $this->_lastName;
  }

  public function setName($_name) {
    $this->_name = $_name;
  }

  public function getAge() {
    return $this->_age;
  }

  public function setAge($_age) {
    $this->_age = $_age;
  }

  public function getSalary() {
    return $this->_salary;
  }

  public function getIncrease() {
    return $this->_increase;
  }

  public function print() {
    echo $this->getFirstName();
    echo "<br>";
    echo $this->getLastName();
    echo "<br>";
    echo $this->getType();
    echo "<br>";
    echo $this->getSalary();
    echo "<br>";
    echo $this->SalaryUp();
    echo "<br>";
  }

}



class Salaried extends Employees {    // récupère toutes les valeurs déclarés //
  protected $_type;
  public function __construct() {
    parent::__construct();
    $_type = "Noob";
    $_randomSalary = intval(rand(800,1200));
    $this->setType($_type);
    $this->randomSalary($_randomSalary);
  }
}

?>
