json.extract! user, :id, :surname, :admin, :created_at, :updated_at
json.url user_url(user, format: :json)
