// Obtenir le modal
var modal = document.getElementById('mymodal');

// Créer le btn qui ouvre le modal
var btn = document.getElementById("mybutton");

// Créer le <span> qui ferme le modal
var span = document.getElementsByClassName("close")[0];

// Créer la fonction qui permet d'ouvrir le modal au click
btn.onclick = function() {
  modal.style.display = "block";
}

// Créer la fonction qui permet de fermer le modal
span.onclick = function() {
  modal.style.display = "none";
}

// Créer une fonction qui ferme le modal quand on click partout sur la page
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
