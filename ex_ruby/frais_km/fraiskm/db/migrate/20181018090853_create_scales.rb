class CreateScales < ActiveRecord::Migration[5.2]
  def change
    create_table :scales do |t|
      t.date :year
      t.string :vehicle_type
      t.text :power
      t.decimal :coefficient

      t.timestamps
    end
  end
end
