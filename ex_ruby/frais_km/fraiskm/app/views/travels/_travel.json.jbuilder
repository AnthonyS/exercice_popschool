json.extract! travel, :id, :start_adress, :end_adress, :distance, :date, :reason, :state, :amount, :created_at, :updated_at
json.url travel_url(travel, format: :json)
