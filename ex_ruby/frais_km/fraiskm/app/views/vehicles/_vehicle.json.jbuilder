json.extract! vehicle, :id, :power, :vehicle_type, :registration, :state, :created_at, :updated_at
json.url vehicle_url(vehicle, format: :json)
