class CreateVehicles < ActiveRecord::Migration[5.2]
  def change
    create_table :vehicles do |t|
      t.text :power
      t.string :vehicle_type
      t.text :registration
      t.boolean :state

      t.timestamps
    end
  end
end
