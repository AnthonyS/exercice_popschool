<?php

$bdd = new PDO('mysql:host=localhost; dbname=exercice_php', 'anthony', 'userpop');

if(isset($_POST['form_registration']))   // formregistration est un try si tout est lié
{
  $pseudo = htmlspecialchars($_POST['pseudo']);   //  "htmlspecialchars" évite tout tentative d'intrusion sécurité
  $mail = htmlspecialchars($_POST['mail']);
  $password = sha1($_POST['password']);   //sha1 permet de sécurisé le mdp, "faible", déconseiller, les Api sont une meilleure alternative

  if(!empty($_POST['pseudo']) AND !empty($_POST['mail']) AND !empty($_POST['password']))
  {
    $pseudoLenght = strlen($pseudo);    // fonction qui vérifie le nb de caractéres
    if ($pseudoLenght <= 45)
    {

      if(filter_var($mail, FILTER_VALIDATE_EMAIL))    // fonction empêchant de changer le type mail en texte
      {
        $reqMail = $bdd->prepare("SELECT * FROM users WHERE mail = ?"); // fonction prepare, execute mail
        $reqMail->execute(array($mail));                                // nécessaire au contrôle du mail
        $mailExist = $reqMail->rowCount();    // rowcount compte le nombre d'info mail
        if($mailExist == 0)
        {

          if($password)
          {
            $insertUsers = $bdd->prepare("INSERT INTO users(pseudo, mail, password) VALUES(?, ?, ?,)");   // prepare  la bdd avant d'execute
            $insertUsers->execute(array($pseudo, $mail, $password));    // insert les valeurs, la bdd sql
            $erreur = " Le compte à été créé !";
          }
          else
          {
            $erreur = "Le pseudo doit contenir 45 caractéres !";
          }
        }
        else
        {
          $erreur = "Adresse mail déjà utilisée !";
        }
      }
      else
      {
        $erreur = "L'adresse mail n'est pas valide !";
      }
    }
    else
    {
      $erreur = "Tous les champs doivent être complétés !";
    }
  }

  ?>



  <html>
  <head>
    <title> Formulaire PHP </title>
    <meta charset="utf-8">
  </head>
  <body>
    <h2> Inscription </h2>
    <form method="POST" action="">
      <p>
        Identifiant : <input type="text" name="pseudo" value="Enter your username"> <br>
        Mail : <input type="email" name="mail" value="Enter your mail"> <br>
        Password : <input type="password" name"password" value="Enter your password"> <br>
        <input type="submit" name="form_registration" value="GO !">
      </p>
    </form>
    <?php
    if(isset($erreur))
    {
      echo $erreur;     // renvoi l'erreur défini
    }
    ?>
  </body>
  </html>
