<?php
session_start();

$bdd = new PDO('mysql:host=localhost; dbname=exercice_php', 'anthony', 'userpop');

if(isset($_POST['form_connect']))
{
  $mailConnect = htmlspecialchars($_POST['mail_connect']);
  $passwordConnect = sha1($_POST['password_connect']);  // Sha1 à nouveau pour récupérer le mdp crypt
  if(!empty($mailConnect) AND !empty($passwordConnect))
  {
    $reqUser = $bdd->prepare("SELECT * FROM users WHERE mail = ? AND password = ?");
    $reqUser->execute(array($mailConnect, $passwordConnect));
    $userExist = $reqUser->rowCount();    // analyse et compte les infos dabs la bdd
    if($userExist == 1)
    {
      $userInfo = $reqUser->fetch();  //  parcoure le résultat de la requête et voit si tout est lié
      $_SESSION['id'] = $userInfo['id'];
      $_SESSION['pseudo'] = $userInfo['pseudo'];
      $_SESSION['mail'] = $userInfo['mail'];    // conserve les variables déclarés et les affiches sur le site
      header("location: profil.php?id=".$_SESSION['id']);   // redirige vers le profil co
    }
    else
    {
      $erreur = "Saisir à nouveau";
    }
  }
  else
  {
    $erreur = "Tous les champs doivent être complétés !";
  }
}

?>


<html>
<head>
  <title> Formulaire PHP </title>
  <meta charset="utf-8">
</head>
<body>
  <h2> Connexion </h2>
  <form method="POST" action="">
    <input type="email" name="mail_connect" placeholder="Mail">
    <input type="password" name="password_connect" placeholder="Password">
    <input type="submit" name="form_connect" value="Se connecter !">
  </form>


  <?php
  if(isset($erreur))
  {
    echo $erreur;     // renvoi l'erreur défini
  }
  ?>
</body>
</html>
