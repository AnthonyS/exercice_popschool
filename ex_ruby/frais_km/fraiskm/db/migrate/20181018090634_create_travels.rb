class CreateTravels < ActiveRecord::Migration[5.2]
  def change
    create_table :travels do |t|
      t.text :start_adress
      t.text :end_adress
      t.integer :distance
      t.datetime :date
      t.text :reason
      t.boolean :state
      t.boolean :amount

      t.timestamps
    end
  end
end
