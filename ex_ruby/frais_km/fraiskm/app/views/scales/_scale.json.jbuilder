json.extract! scale, :id, :year, :vehicle_type, :power, :coefficient, :created_at, :updated_at
json.url scale_url(scale, format: :json)
