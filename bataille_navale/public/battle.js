var plateau =[];
placeBateaux();   // créer les cases déterminant par la suite l'état du jeu

var navires = [0,4,3,2]
var touches = 0;

var finPartie=false;    // indique les etapes de la partie
var nbCoups=0;

var message=[];         // renvoi le message correspondant à la situation
message[0]="A l'eau !";
message[2]="Croiseur touché !";
message[3]="Contre-torpilleur touché !";
message[4]="Sous-marin touché !";

message[6]="Case déjà jouée !";
message[12]="Croiseur coulé !";
message[13]="Contre-torpilleur coulé !";
message[14]="Sous-marin coulé !";
