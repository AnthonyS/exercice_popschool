class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :lastname
      t.string :firstname
      t.text :personnal_number
      t.boolean :role
      t.text :state

      t.timestamps
    end
  end
end
