<!-- Exercice 2 :
Faire un système de personnage de jeux vidéos:
- 1 personnage est décrit par :
- 1 race (nain, elfe...)
- 1 rôle (geurrier, mage,...)
- Des forces et des faiblesses (fort au corps à corps, fort dans les attaques à distances, faible face au feu ...)
- Il peut lancer 3 attaques différentes ( boule feu, coup au cac, posture defensive)
Pour lancer les attaques une seule méthode doit être faite, cette méthode lance aléatoirement une des trois attaques.
Il est aussi possible de faire combattre des personnages, vous devez être forces de proposition sur les règles du combats et la facon dont le combat se déroule. -->

<?php

class Characters {
  protected $_race;
  protected $_role;
  protected $_strengh;
  protected $_weakness;

  public function __construct() {
    $this->randomRace();
    $this->randomRole();
    $this->randomStrengh();
    $this->randomWeakness();
  }
}

  public function randomRace() {
    $_arrayRace = ["Nain","Elfe"];
    $_rand = rand(0,(count($_arrayRace)-1));
    $this->_race = $_arrayRace[$_rand];
  }

  public function randomRole() {
    $_arrayRace = ["Guerrier","Mage"];
    $_rand = rand(0,(count($_arrayRace)-1));
    $this->_role = $_arrayRace[$_rand];
  }

  public function randomStrengh() {
    $this->_strengh = rand (15,30);
  }

  public function randomWeakness() {
    $this->_weakness = rand (5,15);
  }

  public function getRace($_race) {
    $this->_race = $_race;
  }

  public function setRace($_race) {
    $this->_race = $_race;
  }

  public function getRole($_role) {
    $this->_role = $_role;
  }

  public function setRole($_role) {
    $this->_role = $_role;
  }

  public function getStrengh($_strengh) {
    $this->_strengh = $_strengh;
  }

  public function setStrengh($_strengh) {
    $this->_strengh = $_strengh;
  }

  public function getWeakness($_weakness) {
    $this->_weakness = $_weakness;
  }

  public function setWeakness($_weakness) {
    $this->_weakness = $_weakness;
  }
  public function skills() {
    $this->_attack1 = actatt1(rand(5, 15));
    $this->_attack2 = actatt2(rand(2, 20));
    $this->_attack3 = actatt3(rand(1, 3));
  }

  public function action() {
    $_yes = (rand(0,1) == 0 ? true : false);
    if($_yes) {    // activ if
      $this->_attack1 = $this->_strengh + $this->_weakness;
      $this->_attack2 = $this->_strengh + $this->_weakness;
      $this->_attack3 = $this->_strengh + $this->_weakness;
      echo "L'attaque 1 inflige :".$this->_attack1;
      echo "<br>"
      echo "L'attaque 2 inflige :".$this->_attack2;
      echo "<br>";
      echo "L'attaque 3 inflige :".$this->_attack3;

    } else {
      echo "Manquée";
    }

  public function print() {
    echo $this->getRace();
    echo "<br>";
    echo $this->getRole();
    echo "<br>";
    echo $this->getStrengh();
    echo "<br>";
    echo $this->getWeakness();
  }
}

$Characters = new Characters();
  
?>



<!-- First try


  class War extends Characters {
   protected $_strengh = 20;
    protected $_weakness = 5;
    public function __construct($_strengh, $_weakness) {
      $this->_strengh=$_strengh;
      $this->_weakness=$_weakness;
    }
    public function print() {
      echo "<p> Force : $this->_strengh, Faiblesse : $this->_weakness, </p>";
    }
  }

  class SpellCasters extends Characters {
    protected $_strengh = 18;
    protected $_weakness = 2;
    public function __construct($_strengh, $_weakness) {
      $this->_strengh=$_strengh;
      $this->_weakness=$_weakness;
    }
    public function print() {
      echo "<p> Force : $this->_strengh, Faiblesse : $this->_weakness, </p>";
    }
  }

  class Tank extends Characters {
    protected $_strengh = 30;
    protected $_weakness = 5;
    public function __construct($_strengh, $_weakness) {
      $this->_strengh=$_strengh;
      $this->_weakness=$_weakness;
    }
    public function print() {
      echo "<p> Force : $this->_strengh, Faiblesse : $this->_weakness, </p>";
    }
  }

  class Fight extends Characters {
  protected $_role='Guerrier';    -->



















}
