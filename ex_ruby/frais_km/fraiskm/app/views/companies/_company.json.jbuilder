json.extract! company, :id, :name, :state, :created_at, :updated_at
json.url company_url(company, format: :json)
