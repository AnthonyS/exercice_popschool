Rails.application.routes.draw do
  devise_for :users
  root 'users#index'
  resources :companies
  resources :scales
  resources :travels
  resources :vehicles
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
