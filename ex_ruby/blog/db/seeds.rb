# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

(1..20).each do |_u|
  User.create surname: Faker::Name.name, admin: Faker::Boolean.boolean
end if User.all.size.zero? #-> permet de ne pas re-enregistrer les Users


#t.string :title
#t.string :description
#t.text :content
#t.references :user, foreign_key: true
#t.datetime :published_at


(1..100).each do |_p|
  Post.create title:        Faker::Book.title,
              description:  Faker::Lorem.paragraph,
              content:      Faker::Lorem.paragraph(4),
              user_id:      rand(20),
              published_at: Faker::Date.backward(140)
end if User.all.size.zero? #-> permet de ne pas re-enregistrer les Users


(1..100).each do |_p|
  Comment.create post_id:      rand(100),
                 user_id:      rand(20),
                 content:      Faker::Lorem.paragraph
end
