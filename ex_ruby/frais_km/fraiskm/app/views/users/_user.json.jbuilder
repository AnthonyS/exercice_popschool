json.extract! user, :id, :lastname, :firstname, :personnal_number, :role, :state, :created_at, :updated_at
json.url user_url(user, format: :json)
